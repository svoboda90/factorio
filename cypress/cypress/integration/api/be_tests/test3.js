import {BankResources, PlayerBuildAction, PlayerMineAction, PlayerName, ResourceType,} from "../../../support/objects";

import {
    getResourceValue,
    sendUserBuildActionWithAutomaticCoordinates,
    sendUserMineActionWithAutomaticCoordinates,
} from "../../../support/factorioClient";

context('Backend Tests', () => {
    beforeEach(() => {
        cy.apiRestartApp()
    });

    it('one player', async () => {

        while (await getResourceValue(BankResources.STONE) <= 10) {
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.HELSINKY, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.DENVER, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.BERLIN, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.NAIROBI, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.MOSCOW, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.OSLO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.RIO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.TOKIO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            cy.wait(1000);
        }

        sendUserMineActionWithAutomaticCoordinates(PlayerName.NAIROBI, PlayerMineAction.BUILD_MINE, ResourceType.COAL)

        sendUserMineActionWithAutomaticCoordinates(PlayerName.HELSINKY, PlayerMineAction.BUILD_MINE, ResourceType.IRON_ORE)

        sendUserMineActionWithAutomaticCoordinates(PlayerName.DENVER, PlayerMineAction.BUILD_MINE, ResourceType.COPPER_ORE)

        sendUserMineActionWithAutomaticCoordinates(PlayerName.BERLIN, PlayerMineAction.BUILD_MINE, ResourceType.OIL)

        buildFirstLevelFactories()

        buildSecondLevelFactories()

        buildThirdLevelFactories()

        buildFourthLevelFactories()


        async function buildFourthLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_ROCKET_SHELL_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_COMPUTER_FACTORX)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_ROCKET_FURL_FACTORY)
            cy.wait(2000);
        }

        async function buildThirdLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_STEEL_CASING_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_CIRCUIT_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_SOLID_FUEL_FACTORY)
            cy.wait(2000);
        }

        async function buildSecondLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_STEEL_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_WIRE_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_FUEL_FACTORY)
            cy.wait(2000);
        }

        async function buildFirstLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_IRON_PLATE_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_COPPER_PLATE_FACTORY)
            cy.wait(2000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_PETROLEUM_FACTORY)
            cy.wait(2000);
        }
    })
})
;