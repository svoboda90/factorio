import {BankResources, PlayerBuildAction, PlayerMineAction, PlayerName, ResourceType,} from "../../../support/objects";

import {
    getSumResourceValue,
} from "../../../support/factorioClient";

context('second Tests', () => {
    beforeEach(() => {
          cy.apiRestartApp()
    });


    it('dostatek zdroju', async () => {

        const warning = "mame dostatek zdroju na dohrani?"
        // const minimumResValue = 3 * 3 * 3 * 3 * 100;
        const minimumResValue = 3 * 3 * 3 * 10;

        cy.log(minimumResValue)
        assert( await getSumResourceValue(ResourceType.STONE) > minimumResValue, warning)
        assert( await getSumResourceValue(ResourceType.OIL) > minimumResValue, warning)
        assert( await getSumResourceValue(ResourceType.COPPER_ORE) > minimumResValue, warning)
        assert( await getSumResourceValue(ResourceType.IRON_ORE) > minimumResValue, warning)
        assert( await getSumResourceValue(ResourceType.COAL) > minimumResValue, warning)
    })
});



