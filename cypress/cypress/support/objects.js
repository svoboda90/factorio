export const ServerUrl = {
    BE_SERVER: "http://localhost:8080/",
    FE_SERVER: "http://localhost:8000/",
};

export const Endpoint = {
    BANK: "bank",       //GET/Provides information about current bank (resources players have)
    WORLD: "world",     //GET/Provides information about current state of world (map / tiles)
    TURNS: "turns",     //POST/Adds a new player action to his/her queue (only one turn is evaluated per tick)
    RESTART: "restart",  //POST/Starts a new game
};

export const RestMethod = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
    PATCH: "PATCH",
};

export const PlayerName = {
    NAIROBI: "Nairobi",
    TOKIO: "Tokyo",
    BERLIN: "Berlin",
    DENVER: "Denver",
    HELSINKY: "Helsinki",
    RIO: "Rio",
    MOSCOW: "Moscow",
    OSLO: "Oslo",
    PROFESSOR: "Professor",
};

export const PlayerMineAction = {
    MINE_RESOURCE: "MineResource",
    BUILD_MINE: "BuildMine",
};

export const PlayerBuildAction = {
    BUILD_IRON_PLATE_FACTORY: "BuildIronPlateFactory",
    BUILD_STEEL_FACTORY: "BuildSteelFactory",
    BUILD_STEEL_CASING_FACTORY: "BuildSteelCasingFactory",
    BUILD_ROCKET_SHELL_FACTORY: "BuildRocketShellFactory",
    BUILD_COPPER_PLATE_FACTORY: "BuildCopperPlateFactory",
    BUILD_WIRE_FACTORY: "BuildWireFactory",
    BUILD_CIRCUIT_FACTORY: "BuildCircuitFactory",
    BUILD_COMPUTER_FACTORX: "BuildComputerFactory",
    BUILD_PETROLEUM_FACTORY: "BuildPetroleumFactory",
    BUILD_FUEL_FACTORY: "BuildBasicFuelFactory",
    BUILD_SOLID_FUEL_FACTORY: "BuildSolidFuelFactory",
    BUILD_ROCKET_FURL_FACTORY: "BuildRocketFuelFactory",
    DESTROY_BUILDING: "DestroyBuilding",
};

export const ResourceType = {
    STONE: "Stone",
    COAL: "Coal",
    IRON_ORE: "IronOre",
    COPPER_ORE: "CopperOre",
    OIL: "Oil",
};

export const BankResources = {
    ...ResourceType,
    BASIC_FUEL: "BasicFuel",
    SOLID_FUEL: "SolidFuel",
    STEEL: "Steel",
    STEEL_CASING: "SteelCasing",
    COPPER_PLATE: "CopperPlate",
    WIRE: "Wire",
    ROCKET_SHELL: "RocketShell",
    ROCKET_FUEL: "RocketFuel",
    IRON_PLATE: "IronPlate",
    CIRCUIT: "Circuit",
    COMPUTER: "Computer",
    PETROLEUM: "Petroleum",
    NONE: "None",
};
