import {BankResources, Endpoint, RestMethod, ServerUrl,} from './objects'

//ok
export function sendUserAction(playerName, action, coordinateX, coordinateY) {
    return new Promise((resolve) => {
        cy.request(RestMethod.POST, ServerUrl.BE_SERVER + Endpoint.TURNS,
            {
                PlayerName: playerName,
                action: action,
                coordinates: {
                    X: coordinateX, Y: coordinateY
                }
            }
        ).then((response) => {
            resolve(response);
        })
    })
}

//ok
export function sendUserBuildActionWithAutomaticCoordinates(playerName, action) {
    return new Promise(async (resolve) => {
        let coordinates = await getFirstEmptyResourcePosition(BankResources.NONE);
        cy.request(RestMethod.POST, ServerUrl.BE_SERVER + Endpoint.TURNS,
            {
                PlayerName: playerName,
                action: action,
                coordinates: {
                    X: coordinates.x, Y: coordinates.y
                }
            }
        ).then((response) => {
            resolve(response);
        })
    })
}

//ok
export function sendUserMineActionWithAutomaticCoordinates(playerName, playerMineAction, resourceType) {
    return new Promise(async (resolve) => {
        let coordinates = await getFirstEmptyResourcePosition(resourceType);
        cy.request(RestMethod.POST, ServerUrl.BE_SERVER + Endpoint.TURNS,
            {
                PlayerName: playerName,
                action: playerMineAction,
                coordinates: {
                    X: coordinates.x, Y: coordinates.y
                }
            }
        ).then((response) => {
            resolve(response);
        })
    })
}

//ok
export function restartServer() {
    cy.request(RestMethod.POST, ServerUrl.BE_SERVER + Endpoint.RESTART
    ).then((response) => {
        logResponseToConsole(response);
    })
}

//ok
export function getWorldMap() {
    return new Promise((resolve) => {
        cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.WORLD
        ).then((response) => {
            resolve(response);
        })
    })
}

//ok
export function getFirstEmptyResourcePosition(resourceName) {
    if (Object.values(BankResources).includes(resourceName)) {
        return new Promise((resolve) => {
            cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.WORLD
            ).then((response) => {
                var body = response.body;
                var obj = JSON.parse(body);

                for (var i = 0; i < obj.tiles.length; i++) {
                    for (var j = 0; j < obj.tiles[i].length; j++) {
                        if (obj.tiles[i][j].resourceSite.Resource == resourceName && obj.tiles[i][j].building == "None") {
                            resolve({x: j, y: i})
                        }
                    }
                }
                resolve(undefined)
            });
        })
    } else {
        throw new Error(`Unknown resource name ${resourceName}`);
    }
}

export function getFirsResourcePosition(resourceName) {
    if (Object.values(BankResources).includes(resourceName)) {
        return new Promise((resolve) => {
            cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.WORLD
            ).then((response) => {
                var body = response.body;
                var obj = JSON.parse(body);

                for (var i = 0; i < obj.tiles.length; i++) {
                    for (var j = 0; j < obj.tiles[i].length; j++) {
                        if (obj.tiles[i][j].resourceSite.Resource == resourceName) {
                            resolve({x: j, y: i})
                        }
                    }
                }
                resolve(undefined)
            });
        })
    } else {
        throw new Error(`Unknown resource name ${resourceName}`);
    }
}

//nefunkcni
export async function getBank() {
    return new Promise((resolve) => {
        cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.BANK
        ).then((response) => {
            let suma = 0
            var body = response.body;
            var obj = JSON.parse(body);
            for (var i = 0; i < obj.length; i++) {
                let resource = obj[i];
                console.log("parse: ", resource)
            }
            // resolve({resource: obj.Resource, amount: obj.Amount})
             // resolve(1)
            // }
            console.log("***")
            console.log(body)
            resolve(obj)
        })
    })
}

//ok
export function getResourceValue(bankResource) {
    return new Promise((resolve) => {
        cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.BANK
        ).then((response) => {
            var body = response.body;
            var obj = JSON.parse(body);

            for (var i = 0; i < obj.length; i++) {
                const resource = obj[i];

                if (resource.Resource == bankResource) {
                    resolve(resource.Amount)
                }
            }
        })
    })
}

export function getSumResourceValue(resourceName) {
    if (Object.values(BankResources).includes(resourceName)) {
        return new Promise((resolve) => {
            cy.request(RestMethod.GET, ServerUrl.BE_SERVER + Endpoint.WORLD
            ).then((response) => {
                let suma = 0;
                var body = response.body;
                var obj = JSON.parse(body);

                for (var i = 0; i < obj.tiles.length; i++) {
                    for (var j = 0; j < obj.tiles[i].length; j++) {
                        if (obj.tiles[i][j].resourceSite.Resource == resourceName) {
                            suma = suma + obj.tiles[i][j].resourceSite.Amount;
                        }
                    }
                }
                console.log(resourceName,suma)
                resolve(suma)
            });
        })
    } else {
        throw new Error(`Unknown resource name ${resourceName}`);
    }
}

//************logger section******************
export function logResponseToConsole(response) {
    console.log("response: " + JSON.stringify(response.body));
}
