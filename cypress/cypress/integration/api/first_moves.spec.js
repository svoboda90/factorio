context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    it('Init Stone Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstStone().click();
            cy.mineResource();
        }
        cy.findFirstStone().click();
        cy.buildMine();

        cy.tickGame(1000);

        cy.stoneDeposit().should('be.above', 2);
    })

    it('Build Iron Plate Factory!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstIronOre().click();
            cy.mineResource();
        }
        cy.findFirstIronOre().click();
        cy.builIronPlateFactory();

        cy.tickGame(1000);

        cy.stoneDeposit().should('be.above', 20);
    })
});