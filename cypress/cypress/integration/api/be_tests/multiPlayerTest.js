import {BankResources, PlayerBuildAction, PlayerMineAction, PlayerName, ResourceType,} from "../../../support/objects";

import {
    getResourceValue,
    sendUserBuildActionWithAutomaticCoordinates,
    sendUserMineActionWithAutomaticCoordinates,
} from "../../../support/factorioClient";

context('Backend Tests', () => {
    beforeEach(() => {
        cy.apiRestartApp()
    });

    it('one player', async () => {
        //
        // let res = await getResourceValue(BankResources.STONE)
        // assert(res == 0, "chyba")

        //mine first stone
        while (await getResourceValue(BankResources.STONE) <= 5) {
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.HELSINKY, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.DENVER, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.BERLIN, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.NAIROBI, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.MOSCOW, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.OSLO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.RIO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.TOKIO, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            cy.wait(1000);
        }

        //build first stone mile
        sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.BUILD_MINE, ResourceType.STONE)

        for (var i = 0; i < 10; i++) {
            buildMines()
            buildMoreStoneFactories()
            buildMoreStoneFactories()
            buildMoreStoneFactories()
            buildMoreStoneFactories()
            buildMoreStoneFactories()
            buildMoreStoneFactories()
            buildMoreCoalFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMines()
            buildSecondLevelFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMines()
            buildMoreCoalFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMoreCoalFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMines()
            buildSecondLevelFactories()
            buildFirstLevelFactories()
            buildMines()
            buildSecondLevelFactories()
            buildMoreCoalFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMoreCoalFactories()
            buildMines()
            buildThirdLevelFactories()
            buildMines()
            buildThirdLevelFactories()
            buildMines()
            buildMoreCoalFactories()
            buildMines()
            buildFirstLevelFactories()
            buildMines()
            buildMoreCoalFactories()
            buildFourthLevelFactories()
            buildMines()
            buildFourthLevelFactories()
            buildMines()
            buildFourthLevelFactories()
        }

        async function buildFourthLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_ROCKET_SHELL_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_COMPUTER_FACTORX)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_ROCKET_FURL_FACTORY)
            cy.wait(1000);
        }

        async function buildThirdLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_STEEL_CASING_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_CIRCUIT_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_SOLID_FUEL_FACTORY)
            cy.wait(1000);
        }

        async function buildSecondLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_STEEL_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_WIRE_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_FUEL_FACTORY)
            cy.wait(1000);
        }

        async function buildFirstLevelFactories() {
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_IRON_PLATE_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_COPPER_PLATE_FACTORY)
            cy.wait(1000);
            sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_PETROLEUM_FACTORY)
            cy.wait(1000);
        }

        async function buildMines() {
            while (await getResourceValue(BankResources.STONE) <= 20) {
                cy.wait(1000);
            }
            sendUserMineActionWithAutomaticCoordinates(PlayerName.NAIROBI, PlayerMineAction.BUILD_MINE, ResourceType.COAL)
            sendUserMineActionWithAutomaticCoordinates(PlayerName.HELSINKY, PlayerMineAction.BUILD_MINE, ResourceType.IRON_ORE)
            sendUserMineActionWithAutomaticCoordinates(PlayerName.DENVER, PlayerMineAction.BUILD_MINE, ResourceType.COPPER_ORE)
            sendUserMineActionWithAutomaticCoordinates(PlayerName.BERLIN, PlayerMineAction.BUILD_MINE, ResourceType.OIL)
            cy.wait(1000);
        }

        async function buildMoreStoneFactories() {
            for (var i = 0; i < 10; i++) {
                while (await getResourceValue(BankResources.STONE) <= 5) {
                    cy.wait(1000);
                }
                cy.log("kde to jsem")
                sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.BUILD_MINE, ResourceType.STONE)
                cy.wait(1000);
                cy.log("kde to jsem")
            }
        }

        async function buildMoreCoalFactories() {
            for (var i = 0; i < 10; i++) {
                while (await getResourceValue(BankResources.STONE) <= 5) {
                    cy.wait(1000);
                }
                cy.log("kde to jsem")
                sendUserMineActionWithAutomaticCoordinates(PlayerName.TOKIO, PlayerMineAction.BUILD_MINE, ResourceType.COAL)
                cy.wait(1000);
                cy.log("kde to jsem")
            }
        }

        // let pokus = await getResourceValue(BankResources.STONE);
        // console.log(pokus)
        // let pokus2 = await getFirstEmptyResourcePosition(BankResources.STONE);
        // console.log(pokus2)
        // let pokus4 = await sendUserAction(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, pokus2.x, pokus2.y)
        //  console.log(pokus4)
        //
        // let pokus5 = await sendUserBuildActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_CIRCUIT_FACTORY)
        //  console.log(pokus5)
        //
        // // sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.IRON_ORE)
        // // sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.IRON_ORE)
        // // sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.IRON_ORE)
        // // //
        // console.log("----------")
        // let pokus6 = await sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.IRON_ORE)
        // console.log(pokus6)

    })
});






