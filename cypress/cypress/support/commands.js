// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import {
    restartServer,
    sendUserAction,
    getWorldMap,
    getBank,
    getResourceValue,
    getFirstEmptyResourcePosition,
    RestMethod,
    Endpoint,
    ServerUrl,
    PlayerAction,
    PlayerName,
    BankResources,
} from './factorioClient'

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (time, options = {}) => {
    cy.wait(time); // game ticks like that
});

Cypress.Commands.add('stoneDeposit', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('findFirstStone', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Stone Icon"]:first');
});

Cypress.Commands.add('mineResource', (playerName, options = {}) => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', (playerName, options = {}) => {
    cy.get('#action-BuildMine').click();
});

Cypress.Commands.add('findFirstIronOre', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="IronOre Icon"]:first');
});

Cypress.Commands.add('builIronPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildIronPlateFactory').click();
});

Cypress.Commands.add('apiSendUserAction', (playerName, action, coordinateX, coordinateY, options = {}) => {
    sendUserAction(playerName, action, coordinateX, coordinateY);
});

Cypress.Commands.add('apiRestartApp', ( options = {}) => {
   restartServer();
});

Cypress.Commands.add('apiGetWorldMap', ( options = {}) => {
    getWorldMap();
});

Cypress.Commands.add('apiGetBank', ( options = {}) => {
    getBank();
});

Cypress.Commands.add('apiGetResourceValue', (resourceName) => {
    getResourceValue(resourceName);
});

Cypress.Commands.add('apiGetFirstEmptyPosition', (resourceName) => {
    getFirstEmptyResourcePosition(resourceName);
});




