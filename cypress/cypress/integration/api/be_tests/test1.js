import {BankResources, PlayerBuildAction, PlayerMineAction, PlayerName, ResourceType,} from "../../../support/objects";

import {
    getBank,
    getFirsResourcePosition,
    getResourceValue, sendUserAction,
    sendUserMineActionWithAutomaticCoordinates,
} from "../../../support/factorioClient";

context('first Tests', () => {
    beforeEach(() => {
        cy.apiRestartApp()
    });

    it('first test', async () => {

        getBank()

        console.log("--------------------------")
        //given
        while (await getResourceValue(BankResources.STONE) <= 5) {
            await sendUserMineActionWithAutomaticCoordinates(PlayerName.PROFESSOR, PlayerMineAction.MINE_RESOURCE, ResourceType.STONE)
            cy.wait(1000);
        }
        let coordinate = await getFirsResourcePosition(BankResources.STONE);
        cy.log(coordinate.x, coordinate.y)
        sendUserAction(PlayerName.PROFESSOR, PlayerBuildAction.BUILD_IRON_PLATE_FACTORY, coordinate.x, coordinate.y);
        cy.wait(2000);
        sendUserAction(PlayerName.PROFESSOR, PlayerBuildAction.DESTROY_BUILDING, coordinate.x, coordinate.y);
        let coordinate2 = await getFirsResourcePosition(BankResources.STONE);
        cy.log(coordinate2.x, coordinate2.y)

        assert(coordinate.x == coordinate2.x && coordinate.y == coordinate2.y, "ok")
        getBank()
    })
});




